/**
 * Last edited by Carlyn on 9/25/15.
 */

//TODO: require only once for all files

var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
//    BSON = require('mongodb').pure().BSON,
    assert = require('assert');
var mongo = require('mongodb');
var http = require('http');
var moment = require('moment');moment().format();

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db_tracker_scoring = new Db('tracker_scoring', server);

db_tracker_scoring.open(function(err, db_tracker_scoring) {
    if(!err) {
        console.log("Connected to 'tracker_scoring' database");
        db_tracker_scoring.collection('scores', {strict:true}, function(err, collection) {
            if (err) {
                console.log("Nothing in 'scores' collection yet.");
            }
        });
    }
});



exports.findById = function(req, res) {
    var id = req.params.id;

    console.log('Retrieving score: ' + id);
    db_tracker_scoring.collection('scores', function(err, collection) {
        collection.findOne({'_id':id}, function(err, item) {
            res.send(item);
        });
    });
};




exports.findAll = function(req, res) {
    db_tracker_scoring.collection('scores', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};




//enter a new score
exports.addScore = function(req, res) {
    var new_score_entry = req.body;
    db_tracker_scoring.collection('scores', function(err, collection) {
        collection.insert(new_score_entry, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                res.send('Posted score ID: '  + result["ops"][0]["_id"].toString())
            }
        });
    });

}

exports.deleteScore = function(req, res) {
    var id = req.params.id;
    console.log('Deleting score: ' + id);
    db_tracker_scoring.collection('scores', function(err, collection) {
        collection.remove({'_id':id}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(result);
            }
        });
    });
}


