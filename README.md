# Installation
    git clone git@bitbucket.org:golfstreamdev/simple_server.git && simple_server

In the root project directory, install all packages:

     npm install

Start mongo database on port 27107. Use --dbpath to specify database path
    
    mongod  --port 27107


If you want  start with an existing database use mongorestore to load the db in data/dump from your root project directory.:

    mongorestore data/dump/

>The dump includes data from this [google doc spreadsheet](https://docs.google.com/spreadsheets/d/1h1hZ7pGC6-oD7G3XVKbj9C15fTHlj7csMOTi2y0oI_w/edit#gid=0). Data are inserted in a collection called 'scores' in database 'tracker_scoring.' 

Start simple_server web service

     npm start

# For tracker scoring log:


### Insert a new entry

     curl -X POST -H 'Content-Type: application/json' -d '{"_id":"999","date": "2015_09_14","game time":"11_55_32","num players":3,"correct":18,"incorrect":0,"t correct":18,"t incorrect":0,"overall scoring":1.0000,"notes":""}' http://localhost:3000/new_score

>A unique ID will be assigned to this entry if "_id" is not specified.


### Get by ID
To get the details for a specific ID, use the endpoint

    curl 'http://localhost:3000/get_score/<scoreID>'

>For instance, to retrieve details for "_id":"999" use 'http://localhost:3000/get_score/999'

### Get all scores:

To get all entries in the 'scores' collection:

     curl 'http://localhost:3000/all_scores'

### Delete score:

To delete a specific entry use endpoint

    curl -X DELETE localhost:3000/delete_score/<scoreID>'

>For instance, to delete "_id":"999" use 'http://localhost:3000/delete_score/999'

# File uploads 

One example video (*.ogg) has been preloaded in the file system. To recover the video use the following:

     curl localhost:3000/download/6d4ab30650bbde6580d784f2f251c7e7 >output.ogg

To upload videos point your browser to http://localhost:3000/uploads  and use the form to locate and upload your file. Your file will be written into the project directory "public/assets" 

> Note that the names of your files are changed to maintain uniqueness.

Alternatively you can copy your file directly into the public/assets directory for storage.


***If these endpoints will be useful we can upgrade this to use mongo gridfs, which will be helpful for storing metadata.


## Tracking.js

Point browser to http://localhost:3000/color_fish_tank.html and allow browser to access camera.

srv0: 192.168.1.205